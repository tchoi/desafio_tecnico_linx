# Simulador de Caixa Eletrônico

Projeto desenvolvido para simular um caixa eletrônico

## Instalação

Clonar esse repositorio

```bash
git clone git@bitbucket.org:tchoi/desafio_tecnico_linx.git
```

Executar o docker compose

```bash
docker-compose up -d
```

Ao finalizar, executar passos abaixo

```bash
docker exec -it desafio-tecnico-linx.test bash
cp .env.example .env
composer install
chmod 777 -R storage/
php artisan migrate
php artisan swagger-lume:generate

```
## License
[MIT](https://choosealicense.com/licenses/mit/)
