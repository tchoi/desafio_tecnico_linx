<?php


namespace App\Repositories\Contracts;


/**
 * Interface ContaRepositoryInterface
 * @package App\Repositories\Contracts
 */
interface ContaRepositoryInterface
{
    /**
     * Busca uma conta pelo ID
     * @param int $id
     * @return mixed
     */
    public function get(int $id);

    /**
     * Busca as contas de um usuario
     * @param int $userId
     * @return mixed
     */
    public function getByUserID(int $userId);

    /**
     * Cria um nova conta
     * @param int $userId
     * @param string $tipoConta
     * @param float $saldo
     * @return mixed
     */
    public function insert(int $userId, string $tipoConta, float $saldo);

    /**
     * Atualiza uma conta existente
     * @param object $conta
     * @param int $userId
     * @param string $tipoConta
     * @param float $saldo
     * @return mixed
     */
    public function update(object $conta, int $userId, string $tipoConta, float $saldo);

    /**
     * Atualiza o saldo de uma conta
     * @param object $conta
     * @param float $saldo
     * @return mixed
     */
    public function atualizaSaldo(object $conta, float $saldo);

    /**
     * Exclui uma conta existente
     * @param object $conta
     * @return mixed
     */
    public function delete(object $conta);

    /**
     * Busca as contas por usuario e conta
     * @param int $userId
     * @param int $contaId
     * @return mixed
     */
    public function getByUserIDAndContaId(int $userId, int $contaId);
}
