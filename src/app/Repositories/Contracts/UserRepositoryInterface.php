<?php

namespace App\Repositories\Contracts;

use Carbon\Carbon;

interface UserRepositoryInterface
{
    /**
     * Busca um usuario pelo ID
     * @param int $user_id
     * @return mixed
     */
    public function get(int $userId);

    /**
     * Cria um novo usuario
     * @param string $nome
     * @param string $cpf
     * @param Carbon $dtNasc
     * @return mixed
     */
    public function insert(string $nome, string $cpf, Carbon $dtNasc);

    /**
     * Atualiza um usuario existente
     * @param object $user
     * @param string $nome
     * @param string $cpf
     * @param Carbon $dtNasc
     * @return mixed
     */
    public function update(object $user, string $nome, string $cpf, Carbon $dtNasc);

    /**
     * Exclui um usuario existente
     * @param object $user
     * @return mixed
     */
    public function delete(object $user);

    /**
     * Busca usuarios
     * @param array $param
     * @return mixed
     */
    public function search(array $param);
}
