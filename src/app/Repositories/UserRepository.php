<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Contracts\UserRepositoryInterface;
use Carbon\Carbon;

class UserRepository implements UserRepositoryInterface
{
    protected User $entity;

    /**
     * UserRepository constructor.
     * @param User $entity
     */
    public function __construct(User $entity)
    {
        $this->entity = $entity;
    }

    public function get(int $userId)
    {
        return $this->entity->where("id", $userId)->first();
    }

    public function insert(string $nome, string $cpf, Carbon $dtNasc)
    {
        return $this->entity->create([
            'nome' => $nome,
            'cpf' => $cpf,
            'dt_nasc' => $dtNasc
        ]);
    }

    public function update(object $user, string $nome, string $cpf, Carbon $dtNasc)
    {
        return $user->update([
            'nome' => $nome,
            'cpf' => $cpf,
            'dt_nasc' => $dtNasc
        ]);
    }

    public function delete(object $user)
    {
        return $user->delete();
    }

    public function search(array $param)
    {
        $regs = $this->entity->whereRaw("1 = 1");
        if (isset($param["nome"])) {
            $regs->where("nome", "LIKE", "%{$param["nome"]}%");
        }
        if (isset($param["dt_nasc"])) {
            $regs->where("dt_nasc", $param["dt_nasc"]);
        }
        if (isset($param["cpf"])) {
            $regs->where("cpf", $param["cpf"]);
        }
        return $regs->paginate();
    }
}
