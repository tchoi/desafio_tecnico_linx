<?php


namespace App\Repositories;


use App\Models\Conta;
use App\Repositories\Contracts\ContaRepositoryInterface;

class ContaRepository implements ContaRepositoryInterface
{
    protected Conta $entity;

    /**
     * ContaRepository constructor.
     * @param Conta $entity
     */
    public function __construct(Conta $entity)
    {
        $this->entity = $entity;
    }


    /**
     * @param int $id
     * @return mixed|void
     */
    public function get(int $id)
    {
        return $this->entity->where("id", $id)->first();
    }

    /**
     * @param int $userId
     * @return mixed|void
     */
    public function getByUserID(int $userId)
    {
        return $this->entity->where("user_id", $userId)->get();
    }

    /**
     * @param int $userId
     * @param string $tipoConta
     * @param float $saldo
     * @return mixed|void
     */
    public function insert(int $userId, string $tipoConta, float $saldo)
    {
        return $this->entity->create([
            'user_id' => $userId,
            'tipo_conta' => $tipoConta,
            'saldo' => $saldo
        ]);
    }

    /**
     * @param object $conta
     * @param int $userId
     * @param string $tipoConta
     * @param float $saldo
     * @return mixed|void
     */
    public function update(object $conta, int $userId, string $tipoConta, float $saldo)
    {
        return $conta->update([
            'user_id' => $userId,
            'tipo_conta' => $tipoConta,
            'saldo' => $saldo
        ]);
    }

    /**
     * @param object $conta
     * @param float $saldo
     * @return mixed|void
     */
    public function atualizaSaldo(object $conta, float $saldo)
    {
        return $conta->update([
            'saldo' => $saldo
        ]);
    }

    /**
     * @param object $conta
     * @return mixed|void
     */
    public function delete(object $conta)
    {
        return $conta->delete();
    }

    public function getByUserIDAndContaId(int $userId, int $contaId)
    {
        return $this->entity->where("id", $contaId)->where("user_id", $userId)->first();
    }
}
