<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nome' => $this->nome,
            'cpf' => $this->cpf,
            'dt_nasc' => Carbon::createFromFormat("Y-m-d H:i:s", $this->dt_nasc)->format('d/m/Y'),
        ];
    }
}
