<?php

namespace App\Http\Requests;

use Anik\Form\FormRequest;

class SaqueConta extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules(): array
    {
        $rules = [
            "user_id" => "required|exists:users,id",
            "conta_id" => "required|exists:contas,id",
            "valor" => "required|min:0|integer"
        ];
        return $rules;
    }
}
