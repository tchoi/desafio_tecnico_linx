<?php

namespace App\Http\Requests;

use Anik\Form\FormRequest;
use LaravelLegends\PtBrValidator\Rules\Cpf;
class StoreUpdateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules(): array
    {
        $rules = [
            "nome" => "required",
            "cpf" => "required|cpf|min:11|max:14|unique:users",
            "dt_nasc" => "required|date_format:d/m/Y"

        ];
        if ($this->method() == 'PUT') {
            $rules["cpf"] = "required|cpf|min:11|max:14";
        }
        return $rules;
    }
}
