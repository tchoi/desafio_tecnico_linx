<?php

namespace App\Http\Requests;

use Anik\Form\FormRequest;

class StoreUpdateConta extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules(): array
    {
        $rules = [
            "user_id" => "required|exists:users,id",
            "tipo_conta" => "required|in:CP,CC",
            "saldo" => "required|min:0"

        ];
        return $rules;
    }
}
