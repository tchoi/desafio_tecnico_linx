<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUpdateConta;
use App\Http\Resources\ContaResource;
use App\Services\ContaService;
use Illuminate\Http\Request;

/**
 * Class ContaController
 * @package App\Http\Controllers
 */
class ContaController extends Controller
{
    /**
     * @var ContaService
     */
    protected ContaService $contaService;

    /**
     * ContaController constructor.
     * @param ContaService $contaService
     */
    public function __construct(ContaService $contaService)
    {
        $this->contaService = $contaService;
    }


    /**
     * @OA\Post(
     *     path="/api/conta",
     *     operationId="/conta/store",
     *     tags={"Contas"},
     *     description="Criacao de Contas",
     *     @OA\Parameter(
     *         name="user_id",
     *         in="query",
     *         description="ID do usuario",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="tipo_conta",
     *         in="query",
     *         description="Tipo da conta (CC = Conta Corrente, CP - Conta Poupanca)",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="saldo",
     *         in="query",
     *         description="Saldo inicial da conta",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response="201",
     *         description="Conta criada",
     *         @OA\JsonContent(
     *              @OA\Property(
     *                  property="id", description="ID da conta", type="number", example=1
     *              ),
     *              @OA\Property(
     *                  property="user_id", description="ID do Usuario", type="numeric", example=1
     *              ),
     *              @OA\Property(
     *                  property="tipo_conta", description="Tipo da Conta", type="string", example="CC"
     *              ),
     *              @OA\Property(
     *                  property="saldo", description="Saldo inicial da conta", type="numeric", example=1000
     *              )
     *         )
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Error: Bad request. When required parameters were not supplied.",
     *     ),
     * )
     */
    public function store(StoreUpdateConta $request)
    {
        $conta = $this->contaService->makeConta($request->all());
        return new ContaResource($conta);
    }

}
