<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @OA\Info(
     *   title="Simulador Caixa Eletronico",
     *   version="1.0",
     *   @OA\Contact(
     *     email="inboxfox@gmail.com",
     *     name="Jonas Ferreira"
     *   )
     * )
     */
}
