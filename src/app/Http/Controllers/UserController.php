<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUpdateUser;
use App\Http\Resources\UserResource;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * @OA\Schema(
 *   schema="UserSchema",
 *   title="Usuario",
 *   description="Usuario",
 *   @OA\Property(
 *     property="id", description="ID do usuario", type="number", example=1
 *  ),
 *   @OA\Property(
 *     property="nome", description="Nome do usuario", type="string", example="Jose da Silva"
 *  ),
 *   @OA\Property(
 *     property="cpf", description="CPF do usuario", type="string", example="72720405485"
 *  ),
 *   @OA\Property(
 *     property="dt_nasc", description="Data de nascimento do usuario", type="string", example="20/02/1987"
 *  )
 * )
 */

class UserController extends Controller
{
    /**
     * @var UserService
     */
    protected UserService $userService;

    /**
     * UserController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @OA\Get(
     *     path="/api/usuario",
     *     operationId="/usuario/index",
     *     tags={"Usuários"},
     *     description="Pesquisa de Usuário",
     *     @OA\Parameter(
     *         name="nome",
     *         in="query",
     *         description="Nome do Usuario",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="cpf",
     *         in="query",
     *         description="Cpf do usuario",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="dt_nasc",
     *         in="query",
     *         description="Data de Nascimento do usuario",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Lista de usuarios",
     *         @OA\JsonContent(
     *              @OA\Property (
     *                  property="data",
     *                  type="array",
     *                  description="List of users",
     *                  @OA\Items(
     *                      ref="#/components/schemas/UserSchema"
     *                  )
     *              )
     *         )
     *     )
     * )
     */
    public function index(Request $request)
    {
        $data = $request->all();
        $users = $this->userService->search($data);
        return UserResource::collection($users);
    }

    /**
     * @OA\Post(
     *     path="/api/usuario",
     *     operationId="/usuario/store",
     *     tags={"Usuários"},
     *     description="Criacao de Usuarios",
     *     @OA\Parameter(
     *         name="nome",
     *         in="path",
     *         description="Nome do Usuario",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="cpf",
     *         in="path",
     *         description="Cpf do usuario",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="dt_nasc",
     *         in="path",
     *         description="Data de Nascimento do usuario",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response="201",
     *         description="Usuário criado",
     *         @OA\JsonContent(
     *              @OA\Property(
     *                  property="id", description="ID do usuario", type="number", example=1
     *              ),
     *              @OA\Property(
     *                  property="nome", description="Nome do usuario", type="string", example="Jose da Silva"
     *              ),
     *              @OA\Property(
     *                  property="cpf", description="CPF do usuario", type="string", example="72720405485"
     *              ),
     *              @OA\Property(
     *                  property="dt_nasc", description="Data de nascimento do usuario", type="string", example="20/02/1987"
     *              )
     *         )
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Error: Bad request. When required parameters were not supplied.",
     *     ),
     * )
     */
    public function store(StoreUpdateUser $request)
    {
        $user = $this->userService->makeUser($request->all());
        return new UserResource($user);
    }


    /**
     * @OA\Get(
     *     path="/api/usuario/{id}",
     *     operationId="/usuario/show",
     *     tags={"Usuários"},
     *     description="Traz informacoes de um usuario",
     *     @OA\Response(
     *         response="200",
     *         description="Usuário",
     *         @OA\JsonContent(
     *              @OA\Property(
     *                  property="id", description="ID do usuario", type="number", example=1
     *              ),
     *              @OA\Property(
     *                  property="nome", description="Nome do usuario", type="string", example="Jose da Silva"
     *              ),
     *              @OA\Property(
     *                  property="cpf", description="CPF do usuario", type="string", example="72720405485"
     *              ),
     *              @OA\Property(
     *                  property="dt_nasc", description="Data de nascimento do usuario", type="string", example="20/02/1987"
     *              )
     *         )
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Error: Bad request. When required parameters were not supplied.",
     *     ),
     * )
     */
    public function show($id)
    {
        $user = $this->userService->getUserById($id);
        if (!isset($user->id)) {
            return $user;
        }
        return new UserResource($user);
    }

    /**
     * @OA\Put(
     *     path="/api/usuario/{id}",
     *     operationId="/usuario/update",
     *     tags={"Usuários"},
     *     description="Atualiza um usuario",
     *     @OA\Parameter(
     *         name="nome",
     *         in="path",
     *         description="Nome do Usuario",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="cpf",
     *         in="path",
     *         description="Cpf do usuario",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="dt_nasc",
     *         in="path",
     *         description="Data de Nascimento do usuario",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Usuário atualizado",
     *         @OA\JsonContent(
     *              @OA\Property(
     *                  property="message", description="retorno", type="string", example="Usuário atualizado"
     *              )
     *         )
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Error: Bad request. When required parameters were not supplied.",
     *     ),
     * )
     */
    public function update(StoreUpdateUser $request, $id)
    {
        return $this->userService->updateUser($id, $request->all());
    }

    /**
     * @OA\Delete(
     *     path="/api/usuario/{id}",
     *     operationId="/usuario/delete",
     *     tags={"Usuários"},
     *     description="Exclui um usuario",
     *     @OA\Response(
     *         response="200",
     *         description="Usuário atualizado",
     *         @OA\JsonContent(
     *              @OA\Property(
     *                  property="message", description="retorno", type="string", example="Usuário atualizado"
     *              )
     *         )
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Error: Bad request. When required parameters were not supplied.",
     *     ),
     * )
     */
    public function destroy($id)
    {
        return $this->userService->deleteUser($id);
    }
}
