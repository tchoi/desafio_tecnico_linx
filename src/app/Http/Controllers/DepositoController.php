<?php

namespace App\Http\Controllers;

use App\Http\Requests\DepositoConta;
use App\Services\OperacoesService;
use Illuminate\Http\Request;

class DepositoController extends Controller
{
    protected OperacoesService $operacoesService;

    /**
     * DepositoController constructor.
     * @param OperacoesService $operacoesService
     */
    public function __construct(OperacoesService $operacoesService)
    {
        $this->operacoesService = $operacoesService;
    }

    /**
     * @OA\Post(
     *     path="/api/deposito",
     *     operationId="/api/deposito",
     *     tags={"Deposito"},
     *     description="Realizacao de deposito",
     *     @OA\Parameter(
     *         name="user_id",
     *         in="query",
     *         description="ID do usuario",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="conta_id",
     *         in="query",
     *         description="ID da Conta",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="valor",
     *         in="query",
     *         description="Valor do saque",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Deposito realizado com sucesso",
     *         @OA\JsonContent(
     *              @OA\Property(
     *                  property="message", description="Mensagem de sucesso", type="string", example="Deposito realizado com sucesso"
     *              )
     *         )
     *     ),
     *     @OA\Response(
     *         response="405",
     *         description="Usuario nao encontrado"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Conta nao encontrada"
     *     )
     * )
     */
    public function depositar(DepositoConta $request)
    {
        return $this->operacoesService->deposito(
            $request->get('user_id'),
            $request->get('conta_id'),
            $request->get('valor')
        );
    }
}
