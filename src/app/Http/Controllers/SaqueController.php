<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaqueConta;
use App\Services\OperacoesService;
use Illuminate\Http\Request;

/**
 * @OA\Schema(
 *   schema="SaqueSchema",
 *   title="SaqueSchema",
 *   description="SaqueSchema",
 *   @OA\Property(
 *     property="100", description="Numero de cedulas de 100", type="number", example=1
 *  ),
 *   @OA\Property(
 *     property="50", description="Numero de cedulas de 100", type="number", example=1
 *  ),
 *   @OA\Property(
 *     property="20", description="Numero de cedulas de 100", type="number", example=1
 *  )
 * )
 */
class SaqueController extends Controller
{
    protected OperacoesService $operacoesService;

    /**
     * DepositoController constructor.
     * @param OperacoesService $operacoesService
     */
    public function __construct(OperacoesService $operacoesService)
    {
        $this->operacoesService = $operacoesService;
    }

    /**
     * @OA\Post(
     *     path="/api/saque",
     *     operationId="/api/saque",
     *     tags={"Saque"},
     *     description="Realizacao de saque",
     *     @OA\Parameter(
     *         name="user_id",
     *         in="query",
     *         description="ID do usuario",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="conta_id",
     *         in="query",
     *         description="ID da Conta",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="valor",
     *         in="query",
     *         description="Valor do saque",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Saldo realizado com sucesso",
     *         @OA\JsonContent(
     *              @OA\Property(
     *                  property="cedulas",
     *                  description="Celulas",
     *                  type="array",
     *                  @OA\Items(
     *                      ref="#/components/schemas/SaqueSchema"
     *                  )
     *              ),
     *              @OA\Property(
     *                  property="message", description="Mensagem de sucesso", type="string", example="Saque realizado com sucesso"
     *              )
     *         )
     *     ),
     *     @OA\Response(
     *         response="405",
     *         description="Usuario nao encontrado"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Conta nao encontrada"
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Saldo insuficiente"
     *     )
     * )
     */
    public function sacar(SaqueConta $request)
    {
        return $this->operacoesService->saque(
            $request->get('user_id'),
            $request->get('conta_id'),
            $request->get('valor')
        );
    }
}
