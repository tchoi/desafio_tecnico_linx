<?php

namespace App\Helpers;

class ArraySanitize
{
    /**
     * Mantem somente os parametros permitidos no array
     * @param array $params
     * @param array $allowed
     * @return array
     */
    public static function sanitizeByArray(array $params, array $allowed)
    {
        $paramsAllowed = [];
        foreach ($allowed as $allow) {
            if (isset($params[$allow])) {
                $paramsAllowed[$allow] = $params[$allow];
            }
        }

        return $paramsAllowed;
    }
}
