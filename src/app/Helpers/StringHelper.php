<?php

namespace App\Helpers;

class StringHelper
{
    public static function sanitizeDoc(string $doc)
    {
        return preg_replace('/[^0-9]/', '', $doc);
    }
}
