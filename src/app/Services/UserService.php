<?php

namespace App\Services;

use App\Helpers\ArraySanitize;
use App\Helpers\StringHelper;
use App\Repositories\Contracts\UserRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class UserService
{
    protected UserRepositoryInterface $userRepository;

    /**
     * UserService constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function search(?array $params)
    {
        $params = ArraySanitize::sanitizeByArray(
            $params,
            ["nome", "cpf", "dt_nasc"]
        );
        if (count($params) < 1) {
            return [];
        }

        return $this->userRepository->search($params);
    }

    public function makeUser(array $user)
    {
        $user['dt_nasc'] = Carbon::createFromFormat("d/m/Y", $user['dt_nasc'])->startOfDay();
        $user['cpf'] = StringHelper::sanitizeDoc($user['cpf']);
        return $this->userRepository->insert(
            $user['nome'],
            $user['cpf'],
            $user['dt_nasc']
        );
    }


    public function updateUser(int $userId, array $params)
    {
        $user = $this->userRepository->get($userId);
        if (!$user) {
            return response()->json(["message" => "Usuario nao encontrado"], 404);
        }
        $params['dt_nasc'] = Carbon::createFromFormat("d/m/Y", $params['dt_nasc'])->startOfDay();
        $params['cpf'] = StringHelper::sanitizeDoc($params['cpf']);

        $this->userRepository->update($user, $params['nome'], $params['cpf'], $params['dt_nasc']);
        return response()->json(["message" => "Usuario atualizado"]);
    }

    public function deleteUser(int $userId)
    {
        $user = $this->userRepository->get($userId);
        if (!$user) {
            return response()->json(["message" => "Usuario nao encontrado"], 404);
        }

        $this->userRepository->delete($user);
        return response()->json(["message" => "Usuario excluido"]);
    }

    public function getUserById(int $id)
    {
        $user = $this->userRepository->get($id);
        if (!$user) {
            return response()->json(["message" => "Usuario nao encontrado"], 404);
        }
        return $user;
    }

}
