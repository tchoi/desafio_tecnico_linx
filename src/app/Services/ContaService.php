<?php


namespace App\Services;


use App\Repositories\Contracts\ContaRepositoryInterface;

/**
 * Class ContaService
 * @package App\Services
 */
class ContaService
{
    /**
     * @var ContaRepositoryInterface
     */
    protected ContaRepositoryInterface $contaRepository;

    /**
     * ContaService constructor.
     * @param ContaRepositoryInterface $contaRepository
     */
    public function __construct(ContaRepositoryInterface $contaRepository)
    {
        $this->contaRepository = $contaRepository;
    }

    /**
     * @param array $conta
     * @return mixed
     */
    public function makeConta(array $conta)
    {
        return $this->contaRepository->insert(
            $conta['user_id'],
            $conta['tipo_conta'],
            $conta['saldo'] ?? 0
        );
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getContaById(int $id)
    {
        return $this->contaRepository->get($id);
    }

    /**
     * @param int $userId
     * @return mixed
     */
    public function getContasByUserId(int $userId)
    {
        return $this->contaRepository->getByUserID($userId);
    }
}
