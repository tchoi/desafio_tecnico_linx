<?php

namespace App\Services;

use App\Repositories\Contracts\ContaRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Support\Facades\Log;

class OperacoesService
{
    protected ContaRepositoryInterface $contaRepository;
    protected UserRepositoryInterface $userRepository;
    public const VALOR_MINIMO_SAQUE = 20;
    public const CEDULAS_DISPONIVEIS = [
        100,
        50,
        20
    ];

    /**
     * OperacoesService constructor.
     * @param ContaRepositoryInterface $contaRepository
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(ContaRepositoryInterface $contaRepository, UserRepositoryInterface $userRepository)
    {
        $this->contaRepository = $contaRepository;
        $this->userRepository = $userRepository;
    }


    public function deposito(int $userId, int $contaId, int $valor)
    {
        $user = $this->userRepository->get($userId);
        if (!$user) {
            return response()->json(["message" => "Usuario nao encontrado"], 405);
        }

        $conta = $this->contaRepository->getByUserIDAndContaId($userId, $contaId);
        if (!$conta) {
            return response()->json(["message" => "Conta nao encontrada"], 404);
        }
        $saldo = $conta->saldo + $valor;

        $this->contaRepository->atualizaSaldo($conta, (float) $saldo);
        return response()->json(["message" => "Deposito realizado com sucesso"]);
    }

    public function saque(int $userId, int $contaId, int $valor)
    {
        $user = $this->userRepository->get($userId);
        if (!$user) {
            return response()->json(["message" => "Usuario nao encontrado"], 405);
        }

        $conta = $this->contaRepository->getByUserIDAndContaId($userId, $contaId);
        if (!$conta) {
            return response()->json(["message" => "Conta nao encontrada"], 404);
        }

        if ($conta->saldo < $valor) {
            return response()->json(["message" => "Saldo insuficiente"], 403);
        }

        if ($valor < self::VALOR_MINIMO_SAQUE) {
            return response()->json(["message" => "Valor minimo de saque: {self::VALOR_MINIMO_SAQUE}"], 403);
        }

        if ($valor == 30) {
            return response()->json(["message" => "Não há celulas disponiveis para esse valor"], 403);
        }

        if (in_array($valor, self::CEDULAS_DISPONIVEIS) !== false) {
            return response()->json([
                "cedulas" => [
                    $valor => 1
                ]
            ]);
        }

        list($cedulas, $valorCalculo) = $this->separadorNotas($valor);

        if ($valorCalculo > 0) {
            return response()->json(["message" => "Não há celulas disponiveis para esse valor"], 403);
        }

        $saldo = $conta->saldo + $valor;

        $this->contaRepository->atualizaSaldo($conta, (float) $saldo);

        return response()->json([
            "message" => "Saque realizado com sucesso",
            "cedulas" => $cedulas
        ]);
    }

    /**
     * @param int $valor
     * @return array
     */
    public function separadorNotas(int $valor): array
    {
        if ($valor < 100) {
            return $this->valoresMenoresQueCem($valor);
        }

        return $this->valoresMaioresQueCem($valor);
    }

    protected function valoresMenoresQueCem($valor)
    {
        $qtdeCedulas50 = intdiv($valor, 50);
        $comparacao = ($valor % 50) / 10;

        if ($comparacao % 2 > 0 && $qtdeCedulas50 > 0) {
            $qtdeCedulas50 = $qtdeCedulas50 - 1;
        }

        $valorAtual = $valor - ($qtdeCedulas50 * 50);
        if ($valorAtual == 0) {
            return [
                [
                    "50" => $qtdeCedulas50
                ],
                0
            ];
        }

        $qtdeCedulas20 = intdiv($valor, 20);
        $restoGeral = ($valor - (($qtdeCedulas50 * 50) + ($qtdeCedulas20 * 20)));

        $cedulas = [];

        if ($qtdeCedulas50 > 0) {
            $cedulas["50"] = $qtdeCedulas50;
        }

        if ($qtdeCedulas20 > 0) {
            $cedulas["20"] = $qtdeCedulas20;
        }
        return [
            $cedulas,
            $restoGeral
        ];
    }

    protected function valoresMaioresQueCem($valor)
    {
        $restoNegados = [1, 3];

        $qtdeCedulas100 = intdiv($valor, 100);
        $comparacao = ($valor % 100) / 10;

        if (in_array($comparacao, $restoNegados) !== false && $qtdeCedulas100 > 0) {
            $qtdeCedulas100 = $qtdeCedulas100 - 1;
        }
        $valorAtual = $valor - ($qtdeCedulas100 * 100);

        if ($valorAtual == 0) {
            return [
                [
                    "100" => $qtdeCedulas100
                ],
                0
            ];
        }

        list($cedulasMenores, $restoGeral) = $this->valoresMenoresQueCem($valorAtual);

        $cedulas = [
            "100" => $qtdeCedulas100
        ];
        if (isset($cedulasMenores["50"])) {
            $cedulas["50"] = $cedulasMenores["50"];
        }
        if (isset($cedulasMenores["20"])) {
            $cedulas["20"] = $cedulasMenores["20"];
        }
        return [
            $cedulas,
            $restoGeral
        ];
    }
}
