<?php

use App\Models\Conta;
use App\Models\User;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ContaTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $faker = \Faker\Factory::create('pt_BR');

        $userParams = [
            'id' => 1,
            'nome' => $faker->name,
            'cpf' => $faker->cpf,
            'dt_nasc' => $faker->date()
        ];
        User::create($userParams);
    }

    public function testShouldCreateConta()
    {
        $contaParams = [
            'user_id' => 1,
            'tipo_conta' => "CC",
            "saldo" => 1000
        ];
        $this->post("api/conta", $contaParams, []);
        $this->seeStatusCode(201);
        $this->seeJsonStructure([
            'data' => [
                'id',
                'user_id',
                'tipo_conta',
                'saldo'
            ]
        ]);
    }

}
