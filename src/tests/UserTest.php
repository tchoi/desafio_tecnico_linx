<?php

use App\Models\User;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UserTest extends TestCase
{

    public function testShouldCreateUser()
    {
        $faker = \Faker\Factory::create('pt_BR');

        $userParams = [
            'nome' => $faker->name,
            'cpf' => $faker->cpf,
            'dt_nasc' => \Carbon\Carbon::now()->format("d/m/Y")
        ];
        $this->post("api/usuario", $userParams, []);
        $this->seeStatusCode(201);
        $this->seeJsonStructure([
            'data' => [
                'id',
                'nome',
                'cpf',
                'dt_nasc'
            ]
        ]);
    }
    public function testShouldUpdateUser()
    {
        $faker = \Faker\Factory::create('pt_BR');

        $userParams = [
            'nome' => $faker->name,
            'cpf' => $faker->cpf,
            'dt_nasc' => \Carbon\Carbon::now()->format("d/m/Y")
        ];
        $this->put("api/usuario/1", $userParams, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'message'
        ]);

    }

    public function testShouldShowUser()
    {
        $this->get("api/usuario/1");
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'data' => [
                'id',
                "nome",
                "cpf",
                "dt_nasc"
            ]
        ]);
    }
    public function testShouldDeleteUser()
    {
        $this->delete("api/usuario/1");
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'message'
        ]);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $faker = \Faker\Factory::create('pt_BR');

        $userParams = [
            'id' => 1,
            'nome' => $faker->name,
            'cpf' => $faker->cpf,
            'dt_nasc' => \Carbon\Carbon::now()->format("Y-m-d 00:00:00")
        ];
        User::create($userParams);
    }
}
