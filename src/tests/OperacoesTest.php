<?php

use App\Models\Conta;
use App\Models\User;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class OperacoesTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $faker = \Faker\Factory::create('pt_BR');

        $userParams = [
            'id' => 1,
            'nome' => $faker->name,
            'cpf' => $faker->cpf,
            'dt_nasc' => $faker->date()
        ];
        User::create($userParams);

        $contaParams = [
            "id" => 1,
            "user_id" => 1,
            "tipo_conta" => "CC",
            "saldo"=>1000
        ];
        Conta::create($contaParams);
    }

    public function testShouldDepositoSuccess()
    {
        $params = [
            'user_id' => 1,
            "conta_id" => 1,
            "valor" => 4000
        ];
        $response = $this->post("api/deposito", $params, []);
        $this->seeStatusCode(200);
        $this->seeJson(["message" => "Deposito realizado com sucesso"]);
    }

    public function testShouldDepositoFailed()
    {
        $params = [
            'user_id' => 1,
            "conta_id" => 1,
            "valor" => 4000.12
        ];
        $response = $this->post("api/deposito", $params, []);
        $this->seeStatusCode(422);
    }

    public function testShouldSaqueWithValueMultSuccess()
    {
        $params = [
            'user_id' => 1,
            "conta_id" => 1,
            "valor" => 200
        ];
        $response = $this->post("api/saque", $params, []);
        $this->seeStatusCode(200);
        $this->seeJson(["message" => "Saque realizado com sucesso", "cedulas" => ["100" => 2]]);
    }

    public function testShouldSaqueWithReturnMoreOneCedulasSuccess()
    {
        $params = [
            'user_id' => 1,
            "conta_id" => 1,
            "valor" => 280
        ];
        $response = $this->post("api/saque", $params, []);
        $this->seeStatusCode(200);
        $this->seeJson(["message" => "Saque realizado com sucesso", "cedulas" => ["100" => 2, "20" => 4]]);
    }

}
